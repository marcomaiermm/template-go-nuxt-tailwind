package core

import (
	"database/sql"
	"fmt"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	handler_item "gitlab.com/qvue/routes"
)

// Server Type
type ServerType struct {
	db     *sql.DB
	router *gin.Engine
	port   string
	host   string
}

// Server Interface
type Server interface {
	RunServer()
}

// Create a new Postgres Database Connection
func DatabaseConnection() *sql.DB {
	connStr := "user=postgrees password=qvue dbname=postgres sslmode=verify-full"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	fmt.Println("----------DATABASE CONNECTION SUCCESSFUL---------- \n ")
	return db
}

// Functiont o create a new Server
func NewServer() *ServerType {
	db := DatabaseConnection()
	router := gin.Default()

	// Default() will allow all origins
	router.Use(cors.Default())

	// Define all routes for the server
	router.GET("/item", handler_item.GetItems)

	return &ServerType{
		db:     db,
		router: router,
		port:   "8080",
		host:   "localhost",
	}
}

// Function to start the server
func RunServer(s *ServerType) {
	s.router.Run(s.host + ":" + s.port)
}
