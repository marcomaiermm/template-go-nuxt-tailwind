package main

import (
	server "gitlab.com/qvue/core"
)

func main() {
	// create a new server instance
	s := server.NewServer()
	//start the server
	server.RunServer(s)
}
