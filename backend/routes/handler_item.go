package routes

import (
	"fmt"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"

	schemas "gitlab.com/qvue/schemas"
)

var lorem = "urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim sed faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras ornare arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim sit amet venenatis urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt augue interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis tincidunt"

var words = []string{}

var Items = []schemas.Item{}

func generateItems() []schemas.Item {
	count := 10000
	c := 0

	var items = []schemas.Item{}

	for c < count {
		// Generate random items
		item := schemas.Item{
			ID: fmt.Sprintf("%d", c),
			// Pick a random word for the title
			Title: words[rand.Intn(len(words))],
			// Pick a random price
			Price: rand.Float64() * 100,
		}
		items = append(items, item)
		c++
	}

	return items
}

func init() {
	rand.Seed(time.Now().UnixNano())

	// create a hashmap of words
	wordsMap := make(map[string]bool)
	for _, word := range strings.Split(lorem, " ") {
		if !wordsMap[word] {
			wordsMap[word] = true
			words = append(words, strings.Replace(word, ".", "", -1))
		}
	}
	// generate actual items
	Items = generateItems()
}

// CRUD - Create, Read, Update, Delete HGandler for Complaint Positions
func GetItems(c *gin.Context) {
	// send the lsit of items to the client
	c.IndentedJSON(http.StatusOK, Items)
}
