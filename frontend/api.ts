import { IObject, IURL } from "./interfaces";

export class APIUrl {
    url: string;

    constructor(url: IURL) {
        this.url = `${url.schema}://${url.host}`;
        if (url.port) {
            this.url += `:${url.port}`;
        }
        // add the route to the url and check if it starts with a slash
        if (url.route.startsWith("/")) {
            this.url += url.route;
        }
        else {
            this.url += `/${url.route}`;
        }
        // Check if the url is valid
        if (!this.isValidUrl(this.url)) {
            throw new Error("Invalid URL");
        }
    }

    private isValidUrl(urlString: string): boolean {
        // Returns true if the provided url is a valid url
        try {
            const url = new URL(urlString);
            // Returns true if protocol is http or https
            return url.protocol === "http:" || url.protocol === "https:";
        } catch (_) {
            // Invalid Url. Therefore we return false
            return false;
        }
    }
}

export class CRUD {
    // Implement a Base Class for CRUD operations
    url: string;
    constructor(url: string) {
        this.url = url;
    }

    public async create(data: IObject): Promise<IObject | null> {
        // Implement the create instance method
        try {
            const response = await fetch(this.url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            });
            const json = (await response.json()) as IObject;
            return json;
        } catch (_) {
            console.error("Error creating instance");
            return null;
        }
    }

    public async createList(data: IObject[]): Promise<IObject[] | null> {
        // Implement the create method for a list of instances
        try {
            const response = await fetch(this.url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            });
            const json = (await response.json()) as IObject[];
            return json;
        } catch (_) {
            console.error("Error creating list");
            return null;
        }
    }

    public async read(id: string): Promise<IObject | null> {
        // Implement the read instance method
        try {
            const response = await fetch(`${this.url}/${id}`);
            const json = (await response.json()) as IObject;
            return json;
        } catch (_) {
            console.error("Error reading instance");
            return null;
        }
    }

    public async readList(): Promise<IObject[] | null> {
        // Implement the read method for a list of instances
        try {
            const response = await fetch(this.url);
            const json = (await response.json()) as IObject[];
            return json;
        } catch (_) {
            console.error("Error reading list");
            return null;
        }
    }

    public async update(id: string, data: IObject): Promise<IObject | null> {
        // Implement the update instance method
        try {
            const response = await fetch(`${this.url}/${id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            });
            const json = (await response.json()) as IObject;
            return json;
        } catch (_) {
            console.error("Error updating instance");
            return null;
        }
    }

    public async updateList(data: IObject[]): Promise<IObject[] | null> {
        // Implement the update method for a list of instances
        try {
            const response = await fetch(this.url, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            });
            const json = (await response.json()) as IObject[];
            return json;
        } catch (_) {
            console.error("Error updating list");
            return null;
        }
    }

    public async delete(id: string): Promise<IObject | null> {
        // Implement the delete instance method
        try {
            const response = await fetch(`${this.url}/${id}`, {
                method: "DELETE"
            });
            const json = (await response.json()) as IObject;
            return json;
        } catch (_) {
            console.error("Error deleting instance");
            return null;
        }
    }

    public async deleteList(data: IObject[]): Promise<IObject[] | null> {
        // Implement the delete method for a list of instances
        try {
            const response = await fetch(this.url, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            });
            const json = (await response.json()) as IObject[];
            return json;
        } catch (_) {
            console.error("Error deleting list");
            return null;
        }
    }

}