import { IApiType } from "@/interfaces";

declare module '#app' {
    interface NuxtApp {
        $api: IApiType;
    }
}

declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $hello: IApiType
    }
}

export { }