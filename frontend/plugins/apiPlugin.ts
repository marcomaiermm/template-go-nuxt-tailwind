import { IApiType } from "@/interfaces";
import { APIUrl, CRUD } from "@/api";
import { APIUrls } from "@/env";

export default defineNuxtPlugin(() => {

    const createApi = () => {
        const api = {} as IApiType;
        for (const [name, urlObj] of Object.entries(APIUrls)) {
            const newApiUrl = new APIUrl({ route: name, ...urlObj });
            api[name] = new CRUD(newApiUrl.url);
        }
        return api
    }

    const api = createApi();

    return {
        provide: {
            api: api
        }
    }
})