import { IObject } from "./objects";

export interface IRoute {
    path: string;
}

export interface IURL {
    schema: string;
    host: string;
    port: number;
    route: string;
}

export interface ICrud {
    create: (data: IObject) => Promise<IObject>;
    createList: (data: IObject[]) => Promise<IObject[]>;
    read: (id: string) => Promise<IObject>;
    readList: () => Promise<IObject[]>;
    update: (id: string, data: IObject) => Promise<IObject>;
    updateList: (data: IObject[]) => Promise<IObject[]>;
    delete: (id: string) => Promise<IObject>;
    deleteList: (data: IObject[]) => Promise<IObject[]>;
}

export interface IApiType {
    [key: string]: ICrud;
}