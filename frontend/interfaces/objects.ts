// Interface for generic nested objects
export interface IObject {
    [key: string | number]: IObject | string | number | boolean | null;
}